<img width="100" alt="logo" src="src/main/resources/static/resources/images/spring-logo-dataflow-mobile.png">

# DESAFIO PANDEMIA
- Estágio DBServer/CI
- Dupla: [Larissa Magistrali](https://gitlab.com/larissamagistrali) e [Marco Cabral](https://gitlab.com/MarcoCabralSantosJr)
- [Enunciado](https://gitlab.com/larissamagistrali/larissa-marco-desafio-pandemia/-/blob/master/trabalho-2020-1-pandemia%20(1).pdf)
- [Sistemas similares](#5)

## Executando o projeto localmente 
Este projeto é um aplicativo [Spring Boot](https://spring.io/guides/gs/spring-boot) construído usando [Maven](https://spring.io/guides/gs/maven/). Você pode construir um arquivo jar e executá-lo a partir da linha de comando:

```
git clone https://gitlab.com/larissamagistrali/larissa-marco-desafio-pandemia.git
cd larissa-marco-desafio-pandemia
./mvnw package
java -jar target/*.jar
```

Você pode ter acesso a aplicação aqui: http://localhost:8080/ . Ou você pode executá-lo diretamente do Maven usando o plugin Spring Boot Maven
```
./mvnw spring-boot:run
```
Ou ainda utilizando uma IDE.


## Organização da dupla 
- Sprints
    - 03/08/2020 - 07/08/2020: Desenvolvimeto das entidades,controladores, repositórios e banco de dados H2.
    - 10/08/2020 - 14/08/2020: Desenvolvimento das interfaces, relatórios.
    - 17/08/2020 - 21/08/2020: Login, testes.
- [Issues](https://gitlab.com/larissamagistrali/larissa-marco-desafio-pandemia/-/issues?scope=all&utf8=%E2%9C%93&state=all)
- [User Stories](#15)

## Views 

### Página inicial 
<img width="70%" src="src/main/resources/static/resources/images/inicial.png">

### Login
<img width="70%" src="src/main/resources/static/resources/images/login.png">

### Unidades básicas de saúde
<img width="70%" src="src/main/resources/static/resources/images/unidades-basicas.png">
<img width="70%" src="src/main/resources/static/resources/images/unidades-basicas2.png">

### Pacientes
<img width="70%" src="src/main/resources/static/resources/images/paciente.png">
<img width="70%" src="src/main/resources/static/resources/images/paciente2.png">

### Funcionários
<img width="70%" src="src/main/resources/static/resources/images/funcionario.png">

### Atendimentos médicos
<img width="70%" src="src/main/resources/static/resources/images/consulta.png">

### Relatório semanal
<img width="70%" src="src/main/resources/static/resources/images/relatorio.png">

## Serviços, bibliotecas adicionais e fontes de consulta
- [Highcharts](https://www.highcharts.com/)

## Ideias de evolução do projeto
- Utilização de banco de dados abertos para compor as estatísticas e relatórios do site.
    - [Unidades Básicas de Saúde](https://dados.gov.br/dataset/unidades-basicas-de-saude-ubs#:~:text=As%20Unidades%20B%C3%A1sicas%20de%20Sa%C3%BAde,necessidade%20de%20encaminhamento%20para%20hospitais)
    - [OpenDataSus](https://opendatasus.saude.gov.br/organization/ministerio-da-saude)
- [API para endereços](https://correiosapi.apphb.com/)
- [API para consultar CPF](https://www.sintegraws.com.br/api/documentacao-api-receita-federal-cpf.php)
- [API de dados sobre o COVID-19 no Brasil](https://github.com/devarthurribeiro/covid19-brazil-api)
- [Consumir webservice gratuito e de alto desempenho para consultar Códigos de Endereçamento Postal (CEP) do Brasil](https://viacep.com.br/)
